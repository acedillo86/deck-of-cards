package com.acf.deck;

/**
 * A Card returned by the Deck, it will only contain the suite and the value
 * from 1(Ace) to 13 (King)
 * @author Abraham Cedillo
 *
 */

public class Card {
	
	// The suite of the card 
	String suite;
	//The numeric value from 1 to 13
	int value;
	
	
	public String getSuite() {
		return suite;
	}
	public void setSuite(String suite) {
		this.suite = suite;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	public String toString()
	{
		return suite + " " + value + "\n";
	}
	
	

}
