package com.acf.deck;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class will perform deck operations: deal, shuffle and create.
 * @author Abraham Cedillo
 *
 */

public class Deck {
	
	
	// suites
	public static final String SUITE_CLUBS = "clubs";
	public static final String SUITE_DIAMONS = "diamonds";
	public static final String SUITE_HEARTS = "hearts";
	public static final String SUITE_SPADES = "spades";
	

	/**
	 * This method will create a Deck of ordinated cards
	 * @return an ArrayList representing the deck.
	 */
	
	public ArrayList<Card> createDeck()
	{
		ArrayList<Card> deck = new ArrayList<Card>();
		
		//Iterate through the 13 cards on each suite
		for (int i = 0; i < 4; i++)
		{
			for (int j = 1; j <= 13; j++ )
			{
				Card card = new Card();
				//Add the suite depending on 'i' position.
				switch(i)
				{
					case 0 :
						card.setSuite(SUITE_CLUBS);
						break;
					case 1 :
						card.setSuite(SUITE_DIAMONS);
						break;
					case 2 :
						card.setSuite(SUITE_HEARTS);
						break;
					case 3 :
						card.setSuite(SUITE_SPADES);
						break;	
				}
				//We set the numeric value
				card.setValue(j);
				//we add the card to the deck
				deck.add(card);
			}
			
		}
		return deck;
	}
	
	/**
	 * This method will shuffle a deck, the Fisher-Yates
	 * method, it goes through the deck, getting a card and swapping it with a 
	 * random one
	 * 
	 * @param deck
	 * @return a Shuffled deck
	 */
	public ArrayList<Card> shuffle(ArrayList<Card> deck)
	{
		
		Random r = new Random();
	    for (int i = deck.size() - 1; i > 0; i--) {
	    	//Random number to get a random card to swap
	    	int index = r.nextInt(i);
	        //Here we get the random card from the deck
	        Card tmp = deck.get(index);
	        //We remove the random card from the deck 
	        deck.remove(index);
	        //Here we take the next card in the deck and instert it into
	        //the position of the random card
	        deck.add(index, deck.get(i-1));
	        //we remove the card we inserted
	        deck.remove(i);
	        //And finally we insert the random card in the next 
	        //position of the deck
	        deck.add(i, tmp);
	    }

		return deck;
	}
	
	/**
	 * This method will deal the card on the top of the deck,
	 * show it and then remove it
	 * @param deck
	 * @return the deck minus the dealt card.
	 */
	
	public ArrayList<Card> dealCard(ArrayList<Card> deck)
	{
		
		try {
			Card dealtCard = deck.get(deck.size()-1);
			//Get the String values to print
			String cardSuite = dealtCard.getSuite();
			String value = "";
			//if the card value has a name (ace and royalties)
			switch(dealtCard.getValue())
			{
				case 1:
					value = "Ace";
					break;
				case 11:
					value = "Jack";
					break;
				case 12:
					value = "Queen";
					break;
				case 13: 
					value = "King";
					break;
				default:
					value = ""+dealtCard.getValue();
					break;
			}
			System.out.println("Card:"+value+" of "+cardSuite);
			deck.remove(deck.size()-1);
		} catch (ArrayIndexOutOfBoundsException iooobe)
		{
			// TODO Auto-generated catch block
			System.out.println("There are no more cards in the deck");
		} 
		return deck;
	}

}
