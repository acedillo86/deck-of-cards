package com.acf.deck;

import java.util.ArrayList;
/**
 * Java class just for testing purposes.
 * @author Abraham Cedillo
 *
 */

public class MainClass {
	
	public static void main(String args[])
	{
		Deck deck = new Deck();
		ArrayList<Card> cards = deck.createDeck();
		// Unshuffled deck
		System.out.println(cards);
		System.out.println("***********************");
		// Shuffled deck
		cards = deck.shuffle(cards);
		System.out.println(cards);
		System.out.println("***********************");
		// Deal cards
		for (int i = 0; i < 52; i++)
		{
			deck.dealCard(cards);
			System.out.println("Cards remaining: "+cards.size());
		}
		
		
	}

}
